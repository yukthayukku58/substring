string = "NMREC internship"
substring = "ec"
index = string.find(substring)
if index != -1:
    print(f"Substring '{substring}' found at index {index}")
else:
    print("Substring not found")
try:
    index = string.index(substring)
    print(f"Substring '{substring}' found at index {index}")
except ValueError:
    print("Substring not found")